package edu.rutgers.androidphotos04;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class PhotoListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Photo> photos;
    private ArrayList<Bitmap> bitmaps;

    public PhotoListAdapter(Context context, ArrayList<Photo> photos) {
        super();

        this.context = context;
        this.photos = new ArrayList<>(photos.size());
        bitmaps = new ArrayList<>(photos.size());

        for(Photo photo : photos) {
            addItem(photo);
        }
    }

    @Override
    public int getCount() {
        return photos.size();
    }

    @Override
    public Object getItem(int position) {
        return photos.get(position);
    }

    public void addItem(Photo photo) {
        photos.add(photo);
        Bitmap bm = photo.getBitmap();

        int width = bm.getWidth();
        int height = bm.getHeight();

        // scale image to be max 250x250 pixels
        if (width > 250 || height > 250) {
            float scale = 250F / Math.max(width, height);

            Matrix matrix = new Matrix();
            matrix.postScale(scale, scale);

            bm = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        }

        bitmaps.add(bm);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(convertView != null) return convertView;

        View gridView = inflater.inflate(R.layout.photo, null);
        ImageView imageView = gridView.findViewById(R.id.image);
        imageView.setImageBitmap(bitmaps.get(position));

        return gridView;
    }
}
