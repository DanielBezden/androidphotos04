package edu.rutgers.androidphotos04;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;

public class Album implements Serializable {
    private String name;
    private ArrayList<Photo> photos;

    public Album(String name) {
        this.name = name;
        photos = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Photo> getPhotos(){
        return photos;
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }
}
