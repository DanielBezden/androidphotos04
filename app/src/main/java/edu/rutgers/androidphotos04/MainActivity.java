package edu.rutgers.androidphotos04;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public static final String DATA_FILE = "data.dat";

    private ListView albumList;
    private static ArrayList<Album> albums;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // floating action button adds an album
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> addEditAlbum(-1, AddEditAlbum.ADD_ALBUM));

        albums = new ArrayList<>();

        // read in albums from data.dat
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(getFilesDir() + File.separator + DATA_FILE))) {
            while(true) {
                albums.add((Album) ois.readObject());
            }
        } catch (EOFException e) {
            // end of file is normal
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        albumList = findViewById(R.id.album_list);
        albumList.setAdapter(new ArrayAdapter<>(this, R.layout.album, albums));
        albumList.setLongClickable(true);

        // show popup menu with "Delete" and "Rename" options on long press
        albumList.setOnItemLongClickListener((parent, view, position, id) -> {
            PopupMenu popup = new PopupMenu(getApplicationContext(), view);
            popup.setOnMenuItemClickListener(item -> {
                switch(item.getItemId()) {
                    case R.id.album_delete:
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle("Confirm delete");
                        builder.setMessage("Are you sure you want to delete album " + albums.get(position).getName() + "?");
                        builder.setPositiveButton("Confirm", ((dialog, which) -> deleteAlbum(position)));
                        builder.setNegativeButton("Cancel", (dialog, which) -> {});
                        builder.show();
                        break;
                    case R.id.album_rename:
                        addEditAlbum(position, AddEditAlbum.RENAME_ALBUM);
                        break;
                }
                return false;
            });

            popup.inflate(R.menu.album_menu);
            popup.show();
            return true;
        });

        albumList.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(MainActivity.this, AlbumActivity.class);
            intent.putExtra(AlbumActivity.ALBUM_POS, position);
            startActivity(intent);
        });
    }

    private void deleteAlbum(int position) {
        albums.remove(position);
        albumList.setAdapter(new ArrayAdapter<Album>(this, R.layout.album, albums));
    }

    private void addEditAlbum(int position, int requestCode) {
        String[] albumNames = new String[albums.size()];
        for(int i = 0; i < albumNames.length; i++) {
            albumNames[i] = albums.get(i).getName();
        }

        Bundle bundle = new Bundle();
        bundle.putStringArray(AddEditAlbum.ALBUM_NAMES, albumNames);
        bundle.putInt(AddEditAlbum.ALBUM_POS, position);

        Intent intent = new Intent(getApplicationContext(), AddEditAlbum.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, requestCode);
    }

    public static ArrayList<Album> getAlbums() {
        return albums;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_CANCELED) return;

        Bundle bundle = data.getExtras();
        String name;
        int pos;

        switch(requestCode) {
            case AddEditAlbum.ADD_ALBUM:
                name = bundle.getString(AddEditAlbum.ALBUM_NAME);
                albums.add(new Album(name));
                break;
            case AddEditAlbum.RENAME_ALBUM:
                name = bundle.getString(AddEditAlbum.ALBUM_NAME);
                pos = bundle.getInt(AddEditAlbum.ALBUM_POS);
                albums.get(pos).setName(name);
                break;
        }

        albumList.setAdapter(new ArrayAdapter<>(this, R.layout.album, albums));
    }

    public void openSearchDialog(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Search for photos");
        builder.setMessage("person/location \"string\" [AND/OR person/location \"string\"]");
        EditText et = new EditText(this);
        builder.setView(et);
        builder.setPositiveButton("Search", ((dialog, which) -> search(et.getText().toString().trim())));
        builder.setNegativeButton("Cancel", (dialog, which) -> {});
        builder.show();
    }

    private void search(String query) {
        String[] tokens = query.split(" ");
        int start1 = 1, end1 = 1, start2, end2;
        String type1, type2, value1, value2;

        if(tokens.length == 0 || (!tokens[0].equals("person") && !tokens[0].equals("location"))) {
            Toast.makeText(this, "Must start with \"person\" or \"location\"", Toast.LENGTH_SHORT).show();
            return;
        }

        type1 = tokens[0];

        if(tokens.length == 1 || !tokens[1].startsWith("\"")) {
            Toast.makeText(this, "Tag value must be in quotation marks", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            while(!tokens[end1++].endsWith("\""));
            end1--;
        } catch(ArrayIndexOutOfBoundsException e) {
            Toast.makeText(this, "Tag value must be in quotation marks", Toast.LENGTH_SHORT).show();
            return;
        }

        StringBuilder sb = new StringBuilder(tokens[start1]);
        for(int i = start1 + 1; i <= end1; i++) {
            sb.append(" " + tokens[i]);
        }

        value1 = sb.toString().substring(1, sb.length() - 1);

        Album searchResults = new Album("Search Results");
        if(tokens.length == end1 + 1) {
            for(Album album : albums) {
                for(Photo photo : album.getPhotos()) {
                    if(type1.equals("person")) {
                        for(String person : photo.getPeople()) {
                            if(person.contains(value1)) {
                                searchResults.getPhotos().add(photo);
                            }
                        }
                    } else {
                        for(String location : photo.getLocations()) {
                            if(location.contains(value1)) {
                                searchResults.getPhotos().add(photo);
                            }
                        }
                    }
                }
            }

            if(searchResults.getPhotos().isEmpty()) {
                Toast.makeText(this, "No results", Toast.LENGTH_SHORT);
            } else {
                Intent intent = new Intent(getApplicationContext(), AlbumActivity.class);
                intent.putExtra(AlbumActivity.SEARCH_RESULTS, searchResults);
                startActivity(intent);
            }
        } else {
            String andOrOr = tokens[end1 + 1];
            if(!andOrOr.equals("AND") && !andOrOr.equals("OR")) {
                Toast.makeText(this, "Must have either AND or OR", Toast.LENGTH_SHORT).show();
                return;
            }

            if(tokens.length == end1 + 2 || (!tokens[end1 + 2].equals("person") && !tokens[end1 + 2].equals("location"))) {
                Toast.makeText(this, "Must start with \"person\" or \"location\"", Toast.LENGTH_SHORT).show();
                return;
            }

            type2 = tokens[end1 + 2];

            if(tokens.length == end1 + 3 || !tokens[end1 + 3].startsWith("\"")) {
                Toast.makeText(this, "Tag value must be in quotation marks", Toast.LENGTH_SHORT).show();
                return;
            }

            start2 = end2 = end1 + 3;

            try {
                while(!tokens[end2++].endsWith("\""));
                end2--;
            } catch(ArrayIndexOutOfBoundsException e) {
                Toast.makeText(this, "Tag value must be in quotation marks", Toast.LENGTH_SHORT).show();
                return;
            }

            sb = new StringBuilder(tokens[start2]);
            for(int i = start2 + 1; i <= end2; i++) {
                sb.append(" " + tokens[i]);
            }

            value2 = sb.toString().substring(1, sb.length() - 1);

            for(Album album : albums) {
                for(Photo photo : album.getPhotos()) {
                    boolean valid1 = false;
                    boolean valid2 = false;

                    if(type1.equals("person")) {
                        for(String person : photo.getPeople()) {
                            if(person.contains(value1)) {
                                valid1 = true;
                                break;
                            }
                        }
                    } else {
                        for(String location : photo.getLocations()) {
                            if(location.contains(value1)) {
                                valid1 = true;
                                break;
                            }
                        }
                    }

                    if(valid1 && andOrOr.equals("OR")) {
                        searchResults.getPhotos().add(photo);
                        continue;
                    }

                    if(type2.equals("person")) {
                        for(String person : photo.getPeople()) {
                            if(person.contains(value2)) {
                                valid2 = true;
                                break;
                            }
                        }
                    } else {
                        for(String location : photo.getLocations()) {
                            if(location.contains(value2)) {
                                valid2 = true;
                                break;
                            }
                        }
                    }

                    if(andOrOr.equals("AND")) {
                        if(valid1 && valid2) {
                            searchResults.getPhotos().add(photo);
                        }
                    } else if(valid2) {
                        searchResults.getPhotos().add(photo);
                    }
                }
            }

            if(searchResults.getPhotos().isEmpty()) {
                Toast.makeText(this, "No results", Toast.LENGTH_SHORT);
            } else {
                Intent intent = new Intent(getApplicationContext(), AlbumActivity.class);
                intent.putExtra(AlbumActivity.SEARCH_RESULTS, searchResults);
                startActivity(intent);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(getFilesDir() + File.separator + DATA_FILE))) {
            for(Album album : albums) {
                oos.writeObject(album);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}