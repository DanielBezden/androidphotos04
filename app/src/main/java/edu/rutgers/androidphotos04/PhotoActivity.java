package edu.rutgers.androidphotos04;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class PhotoActivity extends AppCompatActivity {
    public static final String PHOTO_POS = "photo_pos";

    public static ArrayList<Photo> photos;
    public static ArrayList<Album> albums;
    private Photo photo;
    private int imagePos;

    private ImageView image;
    private ListView peopleTags;
    private ListView locationTags;
    private Button addPButton;
    private Button addLButton;
    private Button movePhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        albums = AlbumActivity.getAlbums();
        photos = AlbumActivity.getPhotos();
        imagePos = getIntent().getIntExtra(PHOTO_POS, 0);
        photo = photos.get(imagePos);

        setTitle(getFileName(photo.getUri()));

        image = findViewById(R.id.photo_display);
        image.setImageBitmap(photo.getBitmap());

        peopleTags = findViewById(R.id.people_list);
        peopleTags.setAdapter(new ArrayAdapter<>(this, R.layout.album, photo.getPeople()));
        peopleTags.setLongClickable(true);
        peopleTags.setOnItemLongClickListener((parent, view, position, id) -> {
            PopupMenu popup = new PopupMenu(getApplicationContext(), view);
            popup.setOnMenuItemClickListener(item -> {
                AlertDialog.Builder builder = new AlertDialog.Builder(PhotoActivity.this);
                builder.setTitle("Confirm delete");
                builder.setMessage("Are you sure you want to delete tag?");
                builder.setPositiveButton("Confirm", ((dialog, which) -> deleteTag(position, 0)));
                builder.setNegativeButton("Cancel", (dialog, which) -> {});
                builder.show();
                return false;
            });

            popup.inflate(R.menu.photo_menu);
            popup.show();
            return true;
        });

        locationTags = findViewById(R.id.location_list);
        locationTags.setAdapter(new ArrayAdapter<>(this, R.layout.album, photo.getLocations()));
        locationTags.setLongClickable(true);
        locationTags.setOnItemLongClickListener((parent, view, position, id) -> {
            PopupMenu popup = new PopupMenu(getApplicationContext(), view);
            popup.setOnMenuItemClickListener(item -> {
                AlertDialog.Builder builder = new AlertDialog.Builder(PhotoActivity.this);
                builder.setTitle("Confirm delete");
                builder.setMessage("Are you sure you want to delete tag?");
                builder.setPositiveButton("Confirm", ((dialog, which) -> deleteTag(position, 1)));
                builder.setNegativeButton("Cancel", (dialog, which) -> {});
                builder.show();
                return false;
            });

            popup.inflate(R.menu.photo_menu);
            popup.show();
            return true;
        });

        addPButton = findViewById(R.id.add_p_tag_button);
        addPButton.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(PhotoActivity.this);
            builder.setTitle("Add Person Tag");
            builder.setMessage("Enter the person tag you would like to add");
            EditText et = new EditText(this);
            builder.setView(et);
            builder.setPositiveButton("Confirm", ((dialog, which) -> addTag(et.getText().toString().trim(), 0)));
            builder.setNegativeButton("Cancel", (dialog, which) -> {});
            builder.show();
        });

        addLButton = findViewById(R.id.add_l_tag_button);
        addLButton.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(PhotoActivity.this);
            builder.setTitle("Add Location Tag");
            builder.setMessage("Enter the location tag you would like to add");
            EditText et = new EditText(this);
            builder.setView(et);
            builder.setPositiveButton("Confirm", ((dialog, which) -> addTag(et.getText().toString().trim(), 1)));
            builder.setNegativeButton("Cancel", (dialog, which) -> {});
            builder.show();
        });

        movePhoto = findViewById(R.id.move_button);
        movePhoto.setOnClickListener(view -> {
            if(albums.size() > 1) {
                AlertDialog.Builder builder = new AlertDialog.Builder(PhotoActivity.this);
                builder.setTitle("Move Photo");

                String[] albumNames = new String[albums.size()];
                for(int i = 0; i < albumNames.length; i++) {
                    albumNames[i] = albums.get(i).getName();
                }

                builder.setItems(albumNames, (dialog, which) -> movePhoto(which));

                builder.setNegativeButton("Cancel", (dialog, which) -> {});
                builder.show();
            } else {
                Toast.makeText(PhotoActivity.this, "No albums to move photo to", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void startSlideshow(View view) {
        Intent intent = new Intent(this, SlideshowActivity.class);
        intent.putExtra(SlideshowActivity.IMAGE_POS, imagePos);
        startActivity(intent);
    }

    public void movePhoto(int albumPos) {
        if(albums.get(albumPos).getPhotos().contains(photo)) {
            Toast.makeText(getApplicationContext(), "Photo already located in album", Toast.LENGTH_SHORT).show();
        } else {
            albums.get(albumPos).getPhotos().add(photo);
            photos.remove(photo);
            photos = albums.get(albumPos).getPhotos();
        }
    }

    public void addTag(String tag, int type) {
        ArrayList<String> tags;
        if(type == 0) {
            tags = photo.getPeople();
        } else {
            tags = photo.getLocations();
        }

        if(tags.contains(tag)) {
            Toast.makeText(getApplicationContext(), "Photo already associated with tag", Toast.LENGTH_SHORT).show();
        } else {
            tags.add(tag);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.album, tags);

            if(type == 0) {
                peopleTags.setAdapter(adapter);
            } else {
                locationTags.setAdapter(adapter);
            }
        }
    }

    public void deleteTag(int position, int type){
        if(type == 0) {
            photo.getPeople().remove(position);
            peopleTags.setAdapter(new ArrayAdapter<>(this, R.layout.album, photo.getPeople()));
        } else {
            photo.getLocations().remove(position);
            locationTags.setAdapter(new ArrayAdapter<>(this, R.layout.album, photo.getLocations()));
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(getFilesDir() + File.separator + MainActivity.DATA_FILE))) {
            for(Album album : albums) {
                oos.writeObject(album);
            }
        } catch (IOException e) {
            Log.e("exception", Log.getStackTraceString(e));
        }
    }

    private String getFileName(Uri uri) {
        String result = null;
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);

        try {
            if (cursor != null && cursor.moveToFirst()) {
                result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
            }
        } finally {
            cursor.close();
        }

        return result;
    }
}