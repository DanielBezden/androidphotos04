package edu.rutgers.androidphotos04;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

public class SlideshowActivity extends AppCompatActivity {
    public static final String IMAGE_POS = "image_pos";

    private static ArrayList<Photo> photos;
    private int imagePos;

    private ImageView imageView;
    private Button previous, next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slideshow);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        photos = PhotoActivity.photos;
        imagePos = getIntent().getExtras().getInt(IMAGE_POS);

        imageView = findViewById(R.id.photo_display);
        imageView.setImageBitmap(photos.get(imagePos).getBitmap());

        previous = findViewById(R.id.btn_prev);
        next = findViewById(R.id.btn_next);

        previous.setClickable(imagePos > 0);
        next.setClickable(imagePos < photos.size() - 1);
    }

    public void previous(View view) {
        imagePos--;
        imageView.setImageBitmap(photos.get(imagePos).getBitmap());
        previous.setClickable(imagePos > 0);
        next.setClickable(true);
    }

    public void next(View view) {
        imagePos++;
        imageView.setImageBitmap(photos.get(imagePos).getBitmap());
        previous.setClickable(true);
        next.setClickable(imagePos < photos.size() - 1);
    }
}