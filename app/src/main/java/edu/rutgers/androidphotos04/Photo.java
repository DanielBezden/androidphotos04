package edu.rutgers.androidphotos04;

import android.graphics.Bitmap;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class Photo implements Serializable {
    private ArrayList<String> locations;
    private ArrayList<String> people;
    private String uri;
    private transient Bitmap bitmap;

    public Photo(Uri uri, Bitmap bitmap) throws URISyntaxException {
        locations = new ArrayList<>();
        people = new ArrayList<>();
        this.uri = uri.toString();
        this.bitmap = bitmap;
    }

    public ArrayList<String> getLocations(){
        return locations;
    }

    public ArrayList<String> getPeople(){
        return people;
    }

    public Uri getUri(){
        return Uri.parse(uri);
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if(!(obj instanceof Photo)) {
            return false;
        }

        return uri.equals(((Photo) obj).getUri().toString());
    }

    @NonNull
    @Override
    public String toString() {
        return uri;
    }
}

