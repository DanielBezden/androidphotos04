package edu.rutgers.androidphotos04;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.GridView;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class AlbumActivity extends AppCompatActivity {
    public static final String ALBUM_POS = "album_pos";
    public static final String PHOTO_POS = "photo_pos";
    public static final String SEARCH_RESULTS = "search_results";
    private static final int PICKFILE_RESULT_CODE = 1;

    private boolean searchResults = false;
    private int albumPos;
    private static ArrayList<Album> albums;
    private Album album;
    private static ArrayList<Photo> photos;

    private GridView photoGrid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        albums = MainActivity.getAlbums();
        albumPos = getIntent().getIntExtra(ALBUM_POS, -1);
        if(albumPos == -1) {
            searchResults = true;
            album = (Album) getIntent().getSerializableExtra(SEARCH_RESULTS);
        } else {
            album = albums.get(albumPos);
        }

        photos = album.getPhotos();

        setTitle(album.getName());

        // generate bitmaps
        for(Photo photo : album.getPhotos()) {
            getContentResolver().takePersistableUriPermission(photo.getUri(), Intent.FLAG_GRANT_READ_URI_PERMISSION);
            try (InputStream inputStream = getContentResolver().openInputStream(photo.getUri())) {
                Bitmap bmp = BitmapFactory.decodeStream(inputStream);
                photo.setBitmap(bmp);
            } catch (IOException e) {
                Log.e("exception", Log.getStackTraceString(e));
            }
        }

        photoGrid = findViewById(R.id.photo_grid);
        photoGrid.setAdapter(new PhotoListAdapter(this, album.getPhotos()));
        photoGrid.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(AlbumActivity.this, PhotoActivity.class);
            intent.putExtra(AlbumActivity.PHOTO_POS, position);
            intent.putExtra(AlbumActivity.ALBUM_POS, albums.indexOf(album));
            startActivity(intent);
        });

        if(!searchResults) {
            photoGrid.setLongClickable(true);
            photoGrid.setOnItemLongClickListener((parent, view, position, id) -> {
                PopupMenu popup = new PopupMenu(getApplicationContext(), view);
                popup.setOnMenuItemClickListener(item -> {
                    AlertDialog.Builder builder = new AlertDialog.Builder(AlbumActivity.this);
                    builder.setTitle("Confirm delete");
                    builder.setMessage("Are you sure you want to delete this photo?");
                    builder.setPositiveButton("Confirm", ((dialog, which) -> deletePhoto(position)));
                    builder.setNegativeButton("Cancel", (dialog, which) -> {});
                    builder.show();
                    return false;
                });
                popup.inflate(R.menu.photo_menu);
                popup.show();
                return true;
            });
        }

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent, "Select a photo"), PICKFILE_RESULT_CODE);
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(!searchResults) {
            album = albums.get(albumPos);
        }

        photoGrid.setAdapter(new PhotoListAdapter(this, album.getPhotos()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == PICKFILE_RESULT_CODE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            try(InputStream inputStream = getContentResolver().openInputStream(uri)) {
                Bitmap bmp = BitmapFactory.decodeStream(inputStream);

                addPhoto(uri, bmp);
            } catch (IOException e) {
                Log.e("exception", Log.getStackTraceString(e));
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void addPhoto(Uri uri, Bitmap bitmap) {
        Photo newPhoto;

        try {
            newPhoto = new Photo(uri, bitmap);
        } catch (URISyntaxException e) {
            Log.e("exception", Log.getStackTraceString(e));
            return;
        }

        for(Photo photo : album.getPhotos()) {
            if (photo.getUri().equals(uri)) {
                Toast.makeText(getApplicationContext(), "Photo already in album", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        album.getPhotos().add(newPhoto);
        photoGrid.setAdapter(new PhotoListAdapter(this, album.getPhotos()));
    }

    public void deletePhoto(int position){
        photos.remove(position);
        photoGrid.setAdapter(new PhotoListAdapter(this, album.getPhotos()));
    }

    public static ArrayList<Photo> getPhotos() {
        return photos;
    }

    public static ArrayList<Album> getAlbums(){
        return albums;
    }

    @Override
    protected void onStop() {
        super.onStop();

        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(getFilesDir() + File.separator + MainActivity.DATA_FILE))) {
            for(Album album : albums) {
                oos.writeObject(album);
            }
        } catch (IOException e) {
            Log.e("exception", Log.getStackTraceString(e));
        }
    }
}