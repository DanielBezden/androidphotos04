package edu.rutgers.androidphotos04;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class AddEditAlbum extends AppCompatActivity {
    private EditText albumNameEditText;

    private String[] albumNames;
    private int albumPos;

    public static final int ADD_ALBUM = 1;
    public static final int RENAME_ALBUM = 2;

    public static final String ALBUM_NAME = "album_name";
    public static final String ALBUM_POS = "album_pos";
    public static final String ALBUM_NAMES = "album_names";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_edit_album);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        albumNameEditText = findViewById(R.id.album_name);

        Bundle bundle = getIntent().getExtras();
        albumNames = bundle.getStringArray(ALBUM_NAMES);
        albumPos = bundle.getInt(ALBUM_POS);

        if(albumPos != -1) {
            albumNameEditText.setText(albumNames[albumPos]);
        }
    }

    public void save(View view) {
        String name = albumNameEditText.getText().toString().trim();

        if(name.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Name cannot be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        for(int i = 0; i < albumNames.length; i++) {
            if(albumNames[i].equals(name) && albumPos != i) {
                Toast.makeText(getApplicationContext(), "An album with that name already exists", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        Bundle bundle = new Bundle();
        bundle.putString(ALBUM_NAME, name);
        bundle.putInt(ALBUM_POS, albumPos);
        Intent intent = new Intent();
        intent.putExtras(bundle);

        setResult(RESULT_OK, intent);
        finish();
    }

    public void cancel(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }
}